/*  SDL Demo - glider.cpp - a small spacecraft going over folded landscapes
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>
#include <math.h>
#include "global.hpp"
#include "quit.hpp"
using namespace std;

//-----------------------------------------------------------------------------

class Landline
{
	//a class defining a set of points with common depth (y), increasing x, 
	//and varying elevation (z)
	public:
		int N, //number of points
			y; //depth
		int *vx, *vz; //arrays of points
		int *nx, *nz; //arrays of nearest points in the next line

		Landline(int y_); //the constructor
		~Landline(); //the destructor
		void create(int y_); //called by the constructor
		bool shift(int ystep, int stpos);
		void draw();
		void connect(Landline* tgtline);
};


Landline::Landline(int y_)
{
	//the constructor for the Landline class
	create(y_);
}


Landline::~Landline()
{
	//the destructor for the Landline class
	delete[] vx; 
	delete[] vz;
	delete[] nx;
	delete[] nz;
}


void Landline::create(int y_)
{
	//called when the need arises to create or recreate a Landline
	N = (rand() % (SCREEN_WIDTH / size_step)) + 2; //number of points
	vx = new int[N];
	vx[0] = 0; //the 1st and the last point are always at screen borders
	vx[N-1] = SCREEN_WIDTH; //hence +2 a few lines before
	//distribute points along x axis at random
	int xstep = floor(SCREEN_WIDTH / N);
	for(int k=1; k<N-1; k++)
	{
		vx[k] = vx[k-1] + xstep + floor(rand() % xstep/5);
	}
	y = y_; //common depth
	vz = new int[N]; //random additions to depth along the z axis
	for(int k=0; k<N; k++)
	{
		vz[k] = rand() % size_step;
	}
	//arrays for coordinates of the nearest point on a neighbour line
	nx = new int[N];
	nz = new int[N];
}


bool Landline::shift(int ystep, int stpos)
{
	//shifts the line down the screen
	y += ystep;

	if(y > SCREEN_HEIGHT) 
	{
		//lower screen border reached
		delete[] vx; //delete the current points
		delete[] vz;
		delete[] nx;
		delete[] nz;
		create(stpos); //and recreate the Landline at the starting position
		return true; //the line has been recreated
	}
	return false; //the line hasn't reached the border yet
}


void Landline::draw()
{
	//draw the Landline
	SDL_SetRenderDrawColor(renderer, colors[COLOR_WHITE][0], 
			colors[COLOR_WHITE][1], colors[COLOR_WHITE][2], 0xFF);
	for(int i=0; i<N-1; i++)
	{
		SDL_RenderDrawLine(renderer, vx[i], vz[i] + y, vx[i+1], vz[i+1] + y);
		//now draw the lines
		//nz[i] + y + size_step supposes that lines are separated by size_step
		//exactly (bugs are possible in the future)
		SDL_RenderDrawLine(renderer, vx[i], vz[i] + y, nx[i], 
				nz[i] + y + size_step);
	}
}


void Landline::connect(Landline* tgtline)
{
	//draws lines connecting every dot in the Landline to the nearest (by the 
	//x axis) dot in the next one Landline (tgtline)
	for(int i=0; i<N; i++)
	{
		if(tgtline == nullptr)
		{
			//no tgtline for Landline #0, just copy own point coordinates
			nx[i] = vx[i];
			nz[i] = vz[i];
		}
		else
		{
			//find the nearest dot in tgtline
			int dist = SCREEN_WIDTH; //maximum distance is the whole screen
			for(int j=0; j<tgtline->N; j++)
			{
				int dist_ = abs(vx[i] - tgtline->vx[j]); //find the distance
				if(dist_ < dist) 
				{
					//compare and replace the coordinates
					dist = dist_;
					nx[i] = tgtline->vx[j];
					nz[i] = tgtline->vz[j];
				}
			}
		}
	}
}


//-----------------------------------------------------------------------------

void glider(int playtime)
{
	//the main function in this file
	const int gnd_lim = 200, //upper limit of ground lines on the screen
		  gnd_n = (SCREEN_HEIGHT - gnd_lim) / size_step; //number of lines
	//the lines array
	Landline* gnd[gnd_n];
	for(int k=0; k<gnd_n; k++)
	{
		//create the lines
		gnd[k] = new Landline(gnd_lim - k*size_step);
		//from the 1st one on, connect each to the previous one
		if(k == 0)
			gnd[k]->connect(nullptr);
		else
			gnd[k]->connect(gnd[k-1]);
		gnd[k]->draw();
	}
	
	//draw everything rep times
	const short int rep = playtime / FRAME_DELAY;
	int k = 0; //counter
	while(k < rep && !quit())
	{
		//fill the window with black
		SDL_SetRenderDrawColor(renderer, colors[COLOR_BLACK][0], 
				colors[COLOR_BLACK][1], colors[COLOR_BLACK][2], 0xFF);
		SDL_RenderClear(renderer);

		for(int k=0; k<gnd_n; k++)
		{
			//shift and draw the lines
			bool recr = gnd[k]->shift(size_step / 10, gnd_lim);
			if(recr)
			{
				//a line has been recreated - connect a neighbour to it
				if(k > 0)
					gnd[k]->connect(gnd[k-1]);
				else
					gnd[k]->connect(gnd[gnd_n-1]);
			}
			gnd[k]->draw();
		}
		//update and wait
		SDL_RenderPresent(renderer);
		SDL_Delay(FRAME_DELAY);

		k++; //increase the counter
	}
}
