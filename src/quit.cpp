/*  SDL Demo - quit.cpp - a simple event handler to quit the demos
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
using namespace std;

bool quit()
{
	SDL_Event ev;

	while(SDL_PollEvent(&ev))
	{
		if(ev.type == SDL_QUIT) //closing cross clicked
			return true;
		//Escape pressed
		else if(ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_ESCAPE)
			return true;
	}

	return false;
}
