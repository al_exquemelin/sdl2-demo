/*  SDL Demo - text.cpp - text output with a font of my own
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <SDL2/SDL.h>
#include "global.hpp"
using namespace std;

char font[43][5][6] = {{"..+++",
                        ".+..+",
                        "+...+",
                        "+++++",
                        "+...+"}, //A

                       {"++++.",
                        "+...+",
                        "++++.",
                        "+...+",
                        "+++++"}, //B

                       {"+++++",
                        "+....",
                        "+....",
                        "+....",
                        "+++++"}, //C

                       {"+++..",
                        "+..+.",
                        "+...+",
                        "+...+",
                        "+++++"}, //D

                       {"+++++",
                        "+....",
                        "+++++",
                        "+....",
                        "+++++"}, //E

                       {"+++++",
                        "+....",
                        "++++.",
                        "+....",
                        "+...."}, //F

                       {"+++++",
                        "+...+",
                        "+....",
                        "+..++",
                        "+++++"}, //G

                       {"+...+",
                        "+...+",
                        "+++++",
                        "+...+",
                        "+...+"}, //H

                       {"+++++",
                        "..+..",
                        "..+..",
                        "..+..",
                        "+++++"}, //I

                       {"...++",
                        "....+",
                        "....+",
                        "+...+",
                        "++++."}, //J

                       {"+...+",
                        "+..+.",
                        "++++.",
                        "+...+",
                        "+...+"}, //K

                       {"++...",
                        "+....",
                        "+....",
                        "+...+",
                        "+++++"}, //L

                       {"++++.",
                        "+.+.+",
                        "+.+.+",
                        "+.+.+",
                        "+.+.+"}, //M

                       {"+...+",
                        "++..+",
                        "+.+.+",
                        "+..++",
                        "+...+"}, //N

                       {"+++++",
                        "+...+",
                        "+...+",
                        "+...+",
                        "+++++"}, //O

                       {"+++++",
                        "+...+",
                        "+++++",
                        "+....",
                        "+...."}, //P

                       {"+++++",
                        "+...+",
                        "+...+",
                        "+..+.",
                        "+++.+"}, //Q

                       {"+++++",
                        "+...+",
                        "++++.",
                        "+...+",
                        "+...+"}, //R

                       {"+++++",
                        "+....",
                        "+++++",
                        "....+",
                        "+++++"}, //S

                       {"+++++",
                        "..+..",
                        "..+..",
                        "..+..",
                        "..+.."}, //T

                       {"+...+",
                        "+...+",
                        "+...+",
                        ".+..+",
                        "..+++"}, //U

                       {"+...+",
                        "+...+",
                        "+...+",
                        ".+.+.",
                        "..+.."}, //V

                       {"+.+.+",
                        "+.+.+",
                        "+.+.+",
                        "+.+.+",
                        ".++++"}, //W

                       {"+...+",
                        "+...+",
                        ".+++.",
                        "+...+",
                        "+...+"}, //X

                       {"+...+",
                        "+...+",
                        "+++++",
                        "....+",
                        "++++."}, //Y

                       {"+++++",
                        "...+.",
                        "..+..",
                        ".+...",
                        "+++++"}, //Z

                       {"..++.",
                        "...+.",
                        "...+.",
                        "...+.",
                        "...+."}, //1

                       {"+++++",
                        "....+",
                        "+++++",
                        "+....",
                        "+++++"}, //2

                       {"+++++",
                        "....+",
                        ".++++",
                        "....+",
                        "+++++"}, //3

                       {"+...+",
                        "+...+",
                        "+++++",
                        "....+",
                        "....+"}, //4

                       {"+++..",
                        "+....",
                        "++++.",
                        "....+",
                        "+++++"}, //5

                       {"+++++",
                        "+....",
                        "++++.",
                        "+...+",
                        "+++++"}, //6

                       {"+++++",
                        "....+",
                        "....+",
                        "....+",
                        "....+"}, //7

                       {"+++++",
                        "+...+",
                        ".+++.",
                        "+...+",
                        "+++++"}, //8

                       {"+++++",
                        "+...+",
                        "+++++",
                        "....+",
                        "+++++"}, //9

                       {"+++++",
                        "+..++",
                        "+.+.+",
                        "++..+",
                        "+++++"}, //0

                       {".....",
                        ".....",
                        ".....",
                        "++...",
                        "++..."}, //.

                       {".....",
                        ".....",
                        ".....",
                        "++...",
                        "+...."}, //,

                       {".....",
                        ".....",
                        "+++++",
                        ".....",
                        "....."}, //-

                       {"++...",
                        "++...",
                        "++...",
                        ".....",
                        "++..."}, //!

                       {"+++++",
                        "....+",
                        "+++++",
                        ".....",
                        "++..."}, //?

                       {"++...",
                        "+....",
                        ".....",
                        ".....",
                        "....."}, //'

                       {".....",
                        ".....",
                        ".....",
                        ".....",
                        "+++++"}}; //_

int find_char(char to_find);


void test(bool period_to_space)
{
	for(int j=0; j<5; j++) //row
	{
		for(int i=0; i<26; i++) //character
		{
			for(int k=0; k<6; k++) //column
			{
				if(font[i][j][k] == '.')
					if(period_to_space) cout << " ";
					else cout << font[i][j][k];
				else
					cout << font[i][j][k];
			}
			cout << " ";
		}
		cout << "\n";
	}
}


int draw_text(char* text, int x, int y, int a)
{
	//draws the string given at x, y
	const int ppc = 5; //char size in points

	for(int k=0; text[k]; k++)
	{
		int x_off = x + k*(ppc + 1)*a, //x offset of a character
			y_off = y; //y offset of a character
		//if inside the screen
		if(x_off >= -a*ppc && x_off < SCREEN_WIDTH && 
				y_off >= -a*ppc && y_off < SCREEN_HEIGHT)
		{
			//find the char number
			int cn = find_char(text[k]);
			if(cn >= 0) //if found
			{
				for(int i=0; i<ppc; i++)
				{
					for(int j=0; j<ppc; j++)
					{
						if(font[cn][i][j] == '+')
						{
							//place a "pixel" here
							SDL_Rect px = {.x = x_off + j*a, 
								.y = y_off + i*a, .w = a, .h = a};
							//TODO: colors should be controlled from 
							//the outside somehow
							if(i == 0)
							{
								SDL_SetRenderDrawColor(renderer, 
									colors[COLOR_LIGHTRED][0], 
									colors[COLOR_LIGHTRED][1],
									colors[COLOR_LIGHTRED][2], 0xFF);
							}
							else if(i <= 2)
							{
								SDL_SetRenderDrawColor(renderer, 
									colors[COLOR_WHITE][0], 
									colors[COLOR_WHITE][1],
									colors[COLOR_WHITE][2], 0xFF);
							}
							else
							{
								SDL_SetRenderDrawColor(renderer, 
									colors[COLOR_LIGHTGREEN][0], 
									colors[COLOR_LIGHTGREEN][1],
									colors[COLOR_LIGHTGREEN][2], 0xFF);
							}

							SDL_RenderFillRect(renderer, &px);
						}
					}
				}
			}
		}
	} 

	return strlen(text)*(ppc + 1)*a + x; //returns the x coordinate 
		//where the text ends, useful for running text
}


int find_char(char to_find)
{
	//finds the char given and returns its number
	char charset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,-!?'_";
	
	unsigned int i=0; 
	while(i < strlen(charset) && to_find != charset[i]) i++;
	
	if(i >= strlen(charset)) //not found
		return -1;
	else //found 
		return i;
}

