/*  SDL Demo - nightride.cpp - a car going through endless streets at night
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <algorithm>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "global.hpp"
#include "quit.hpp"
#include "text.hpp"
using namespace std;

const short int sky_size = 10, //number of colored stripes
				sky_h = 2*size_step*sky_size; //sky height in pixels

void sky(int k);
void roadlines(int k, bool move);
void car(int x, int y);
void histo(int* bh, int n, int k);

//-----------------------------------------------------------------------------

class Building
{
	//a class for foreground and background buildings
	public:
		SDL_Rect block; //the silhouette
		int wnd_n; //number of windows
		SDL_Rect* wnds; //an array of windows

		Building(int x, int w, int h); //the constructor
		~Building(); //the destructor
		void shift(float step, bool morph);
		void draw(const int block_color[3], const int wnds_color[3]);

	private:
		int add_wnds(int w_, int h_);
};


Building::Building(int x_, int w_, int h_)
{
	//the constructor for the Building class
	//create the dark background block;
	//args are in size_step_s, y calculated using h_
	block = {.x = x_*size_step, .y = sky_h - h_*size_step, .w = w_*size_step, 
		.h = h_*size_step};
	
	//create the windows
	wnds = new SDL_Rect[h_*w_];
	wnd_n = add_wnds(w_, h_);
}


int Building::add_wnds(int w_, int h_)
{
	//places windows at random
	int wnd_n = 0;
	//window size based on the building's width (i.e. foreground or background 
	//line)
	int a;
	if(w_ > 1) a = 10;
	else a = 4;

	for(int j=0; j<w_; j++)
	{
		for(int k=0; k<h_; k++)
		{
			if(rand() % 10 < 2)
			{
				//add a window at wnd_n, leaving dark frames
				wnds[wnd_n].x = (block.x + (j + 0.5)*size_step) - (a / 2);
				wnds[wnd_n].y = (block.y + (k + 0.5)*size_step) - (a / 2);
				wnds[wnd_n].w = a;
				wnds[wnd_n].h = a;
				wnd_n++;
			}
		}
	}

	return wnd_n;
}


Building::~Building()
{
	//the destructor for the Building class
	delete[] wnds;
}


void Building::shift(float step, bool morph)
{
	//shifts the building and changes its size and windows pattern if told so
	block.x -= step*size_step; //shift the block and the windows
	for(int i=0; i<wnd_n; i++)
	{
		wnds[i].x -= step*size_step;
	}

	if(block.x <= -block.w) //the block is off the screen
	{
		for(int i=0; i<wnd_n; i++)
		{
			wnds[i].x += (-block.x + SCREEN_WIDTH);
		}
		block.x = SCREEN_WIDTH;

		int w_, h_;
		if(morph)
		{
			//change both width and height
			w_ = rand() % 8 + 2;
			h_ = rand() % 19;
		}
		else
		{
			//change only height 1 or 2 blocks at a time
			w_ = (block.w / size_step);
			h_ = (block.h / size_step);
			if((rand() % 2) - 1) h_ += 2;
			else h_ -= 1;
		}

		//height limits
		if(w_ == 1 && h_ <= 0) h_ = 1;
		else if(h_ < 0) h_ += 2;
		if(h_ > 0.8*sky_size) h_ -= 2;
		//set the size
		block.w = w_ * size_step;
		block.h = h_ * size_step;
		//change the vertical offset
		block.y = sky_h - block.h;
		//recreate the windows
		if(w_*h_ > 0) //in case the block has non-zero size
		{
			delete[] wnds;
			wnds = new SDL_Rect[w_*h_];
			wnd_n = add_wnds(w_, h_);
		}
	}
}


void Building::draw(const int block_color[3], const int wnds_color[3])
{
	//draw the building on surf using block_color and wnds_color
	if(block.w * block.h > 0) //don't draw nonexisting buildings
	{
		SDL_SetRenderDrawColor(renderer, block_color[0], block_color[1], 
				block_color[2], 0xFF);		
		SDL_RenderFillRect(renderer, &block);
		
		for(int i=0; i<wnd_n; i++)
		{
			SDL_SetRenderDrawColor(renderer, wnds_color[0], wnds_color[1], 
					wnds_color[2], 0xFF);		
			SDL_RenderFillRect(renderer, &wnds[i]);
		}
	}
}

//-----------------------------------------------------------------------------

void nightride(int playtime)
{
	//the main function in this file
	//init the array of buildings in the foreground
	const int fg_n = 8; //number of buildings in the foreground
	Building* fg_blds[fg_n];
	for(int i=0; i<fg_n; i++)
	{
		fg_blds[i] = new Building(i*10, 6, 16);
	}
	//and in the background
	const int bg_n = SCREEN_WIDTH / size_step; //number of buildings in the 
	                                           //background
	Building* bg_blds[bg_n];
	for(int i=0; i<bg_n; i++)
	{
		bg_blds[i] = new Building(i, 1, (i / 8));
	}
	
	int carpos[2] = {-18*size_step, 395}; //car position

	//the running line at the bottom
	char running[] = "THE NIGHTRIDE BY AL_EXQUEMELIN, JUNE-JULY 2018. \
FASTEN YOUR SEAT BELT - THIS BEAT WILL LAST FOREVER IF ONLY WE DON'T MISS \
THE GAS STATION.";
	int linepos[2] = {SCREEN_WIDTH, 455}; //line position

	//draw everything rep times
	const short int rep = playtime / FRAME_DELAY;
	int k = 0; //counter
	while(k < rep && !quit())
	{
		//fill the window with the asphalt color
		SDL_SetRenderDrawColor(renderer, colors[COLOR_DARKGRAY][0], 
				colors[COLOR_DARKGRAY][1], colors[COLOR_DARKGRAY][2], 0xFF);
		SDL_RenderClear(renderer);
		
		//draw the sky
		sky(k);

		int tro_time = 36; //inTRO and ouTRO time in reps, a magic 
						   //number defined by the car's size and speed
		if(k > tro_time && k < (rep - tro_time)) //main part
		{
			//draw the white and yellow lines
			roadlines(k, true);
			//shift the buildings
			for(int i=0; i<bg_n; i++)
			{
				bg_blds[i]->shift(0.5, false);
			}
			for(int i=0; i<fg_n; i++)
			{
				fg_blds[i]->shift(1, true);
			}
		}
		else
		{
			roadlines(k, false);
			//intro and outro: shift the car
			carpos[0] += 1.5*0.5*size_step; 
		}
		//draw the buildings
		int bg_h[bg_n]; //used for the histo
		for(int i=0; i<bg_n; i++)
		{
			bg_blds[i]->draw(colors[COLOR_BLACK], colors[COLOR_WHITE]);
			bg_h[i] = bg_blds[i]->block.h; 
		}
		for(int i=0; i<fg_n; i++)
		{
			fg_blds[i]->draw(colors[COLOR_LIGHTGRAY], 
					colors[COLOR_LIGHTYELLOW]);
		}
		//draw the car
		car(carpos[0], carpos[1]);

		//draw the histogram
		histo(bg_h, bg_n, k);
		
		//shift the running line
		linepos[0] -= 1; 
		//and draw it
		int tailpos = draw_text(running, linepos[0], linepos[1], 4);
		if(tailpos < 0)
			linepos[0] = SCREEN_WIDTH; //loop the text

		//update and wait
		SDL_RenderPresent(renderer);
		SDL_Delay(FRAME_DELAY);

		k++; //increase the counter
	}
}


void sky(int k)
{
	//draw the night sky
	static SDL_Rect sky_rect[sky_size];
	static int sky_color[sky_size][3];

	if(k == 0) //set only once
	{
		for(int j=0; j<sky_size; j++)
		{
			sky_rect[j] = {.x = 0, .y = j*(2*size_step), .w = SCREEN_WIDTH, 
				.h = 2*size_step};
			sky_color[j][0] = colors[COLOR_DARKMAGENTA][0] + j*0x11;
			sky_color[j][1] = 0x00;
			sky_color[j][2] = colors[COLOR_DARKMAGENTA][2] + j*0x11;
		}
	}

	for(int j=0; j<sky_size; j++) //draw every call
	{
		//set the new color
		SDL_SetRenderDrawColor(renderer, sky_color[j][0], sky_color[j][1], 
				sky_color[j][2], 0xFF);		
		//and draw the rectangle
		SDL_RenderFillRect(renderer, &sky_rect[j]);
	}
}


void roadlines(int k, bool move)
{
	//draws yellow and white lines on the road
	const short int road_w = 2*size_step, road_offset = size_step / 2, 
		  l_w = size_step / 10;

	SDL_Rect white_lines[2];
	SDL_SetRenderDrawColor(renderer, colors[COLOR_WHITE][0], 
			colors[COLOR_WHITE][1], colors[COLOR_WHITE][2], 0xFF);
	for(int i=0; i<2; i++)
	{
		white_lines[i] = {.x = 0, .y = sky_h + road_offset + i*road_w, 
			.w = SCREEN_WIDTH, .h = l_w + i};
		SDL_RenderFillRect(renderer, &white_lines[i]);
	}

	const short int yl_w = 1.5*size_step, yl_h = size_step / 10, 
		  yl_sp = 3*size_step, yl_n = SCREEN_WIDTH / (yl_w + yl_sp);
	static SDL_Rect yellow_lines[yl_n];
	if(k == 0) //set only once
	{
		for(int j=0; j<yl_n; j++)
		{
			yellow_lines[j] = {.x = yl_sp + (yl_w + yl_sp)*j, 
				.y = sky_h + road_offset + (road_w / 2) - 5, .w = yl_w, 
				.h = yl_h};
		}
	}
	
	//draw yellow lines every tick
	SDL_SetRenderDrawColor(renderer, colors[COLOR_DARKYELLOW][0], 
			colors[COLOR_DARKYELLOW][1], colors[COLOR_DARKYELLOW][2], 0xFF);
	for(int i=0; i<yl_n; i++)
	{
		//shift the lines when not in intro or outro mode
		if(move)
		{
			yellow_lines[i].x -= 0.5*yl_w;
			if(yellow_lines[i].x <= -yl_w) //the line is off the screen
			{
				if(i == 0)
				{
					yellow_lines[i].x = yellow_lines[yl_n-1].x + (yl_w + yl_sp);
				}
				else
				{
					yellow_lines[i].x = yellow_lines[i-1].x + (yl_w + yl_sp);
				}
			}
		}
		SDL_RenderFillRect(renderer, &yellow_lines[i]);
	}
}


void car(int x, int y)
{
	//draws the car at (x, y)
	int s = 10; //size step
	const int b = 16, q = 4; //vertex array sizes
	//draw the body
	float bodyx[b] = {3, 8, 11, 14, 15, 15, 13, 12, 11, 10, 5, 4, 3, 2, 1, 0},
	      bodyy[b] = {0, 0, 1.5, 2, 3, 4, 4, 3.5, 3.5, 4, 4, 3.5, 3.5, 4, 4, 2};
	short int bodyx_[b], bodyy_[b];
	for(int k=0; k<b; k++) //convert the coordinates
	{
		bodyx_[k] = (short int)(bodyx[k]*s + x);
		bodyy_[k] = (short int)(bodyy[k]*s + y);
	}
	filledPolygonRGBA(renderer, bodyx_, bodyy_, b,
		colors[COLOR_DARKRED][0], colors[COLOR_DARKRED][1], 
		colors[COLOR_DARKRED][2], 0xFF);

	//draw the wheels
	for(float j = 3.5; j<12; j+=8)
	{
		float ypos = 4;
		filledCircleRGBA(renderer, x + j*s, y + ypos*s, s, 
			colors[COLOR_BLACK][0],	colors[COLOR_BLACK][1], 
			colors[COLOR_BLACK][2], 0xFF);
		filledCircleRGBA(renderer, x + j*s, y + ypos*s, 0.7*s, 
			colors[COLOR_LIGHTGRAY][0],	colors[COLOR_LIGHTGRAY][1], 
			colors[COLOR_LIGHTGRAY][2], 0xFF);
	}

	//draw the windows, lights, etc.
	float rectx[][q] = {{8, 11, 10, 8},
		                {8, 9.5, 5, 6},
						{5.5, 4.5, 2, 3.2},
	                    {3, 0, 1, 3},
						{14, 15, 14.5, 13.5},
						{1, 0.2, 0.7, 1.5},
						{15, 15, 14.5, 14.5}
	                   };
	float recty[][q] = {{0, 1.5, 1.5, 0},
		                {0.2, 1.5, 1.5, 0.2},
						{0.2, 1.5, 1.5, 0.2},
		                {0, 2, 2, 0},
						{2, 3, 3, 2},
						{4, 2.5, 2.5, 4},
						{3.5, 3.8, 3.8, 3.5}
	                   };
	for(int i=0; i<7; i++)
	{
		short int rectx_[q], recty_[q];
		for(int k=0; k<q; k++) //convert the coordinates
		{
			rectx_[k] = (short int)(rectx[i][k]*s + x);
			recty_[k] = (short int)(recty[i][k]*s + y);
		}
		
		if(i <= 3) //windows
		{
			filledPolygonRGBA(renderer, rectx_, recty_, q,
				colors[COLOR_BLACK][0], colors[COLOR_BLACK][1], 
				colors[COLOR_BLACK][2], 0xFF);
		}
		else if(i == 4) //headlights
		{
			filledPolygonRGBA(renderer, rectx_, recty_, q,
				colors[COLOR_LIGHTYELLOW][0], colors[COLOR_LIGHTYELLOW][1], 
				colors[COLOR_LIGHTYELLOW][2], 0xFF);
		}
		else if(i == 5) //stop lights
		{
			filledPolygonRGBA(renderer, rectx_, recty_, q,
				colors[COLOR_LIGHTRED][0], colors[COLOR_LIGHTRED][1], 
				colors[COLOR_LIGHTRED][2], 0xFF);
		}
		else if(i == 6) //fog lamps or turn signals
		{
			filledPolygonRGBA(renderer, rectx_, recty_, q,
				colors[COLOR_DARKYELLOW][0], colors[COLOR_DARKYELLOW][1], 
				colors[COLOR_DARKYELLOW][2], 0xFF);
		}
	}

}


void histo(int* bh, int n, int k)
{
	//draws a histogram at the top of the screen
	const short int histo_n = 10, //number of bars
		histo_m = 8, //number of pieces in a bar
		histo_skip = 2, //skip between pieces
		histo_w = (SCREEN_WIDTH / histo_n) - (2*histo_skip), //piece width
		histo_h = ((4*size_step) / histo_m) - histo_skip; //piece height
	int max_h = *max_element(bh, bh + n), //maximum building height
		bar_h[histo_n] = {0}; //bar heights in pieces

	//increase the number for the bar that the current building falls into
	for(int i=0; i<n; i++)
	{
		//FIXME: use floor() and ceil() for conversion
		int bar_num = (int)(((float)bh[i] / (max_h + size_step))*histo_n);
		bar_h[histo_n - bar_num - 1]++; //reverse order:
		//high buildings form the "bass"
	}

	//convert absolute values to bar heights in pieces
	for(int i=0; i<histo_n; i++)
	{
		bar_h[i] = (int)(((float)bar_h[i] / histo_m)*histo_m);
	}

	//opacity values
	static short int opac[histo_m][histo_n];
	if(k == 0)
	{
		for(int i=0; i<histo_m; i++)
		{
			for(int j=0; j<histo_n; j++)
			{
				opac[i][j] = 0xFF;
			}
		}
	}

	//draw the bars
	//enable blending
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	for(int i=0; i<histo_n; i++)
	{
		for(int j=0; j<histo_m; j++)
		{
			SDL_Rect piece = {.x = i*(histo_w + 2*histo_skip) + histo_skip,
				.y = (histo_m - j - 1)*(histo_h + histo_skip), 
				.w = histo_w, .h = histo_h,};
			//fading effect
			if(j <= bar_h[i])
			{
				opac[j][i] = 0xFF;
			}
			else if(opac[j][i] > 0)
			{
				opac[j][i] -= 0x11;
			}

			if(j == histo_m - 1)
			{
				SDL_SetRenderDrawColor(renderer, colors[COLOR_LIGHTRED][0], 
						colors[COLOR_LIGHTRED][1], 
						colors[COLOR_LIGHTRED][2], opac[j][i]);
			}
			else if(j == histo_m - 2)
			{
				SDL_SetRenderDrawColor(renderer, colors[COLOR_WHITE][0], 
						colors[COLOR_WHITE][1], colors[COLOR_WHITE][2], opac[j][i]);
			}
			else
			{
				SDL_SetRenderDrawColor(renderer, colors[COLOR_LIGHTGREEN][0], 
						colors[COLOR_LIGHTGREEN][1], 
						colors[COLOR_LIGHTGREEN][2], opac[j][i]);
			}
			SDL_RenderFillRect(renderer, &piece);
		}
	}
}

