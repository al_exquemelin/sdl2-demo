/*  SDL Demo - sdl_demo.cpp - the main file
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <SDL2/SDL.h>
#include "global.hpp"
#include "nightride.hpp"
#include "glider.hpp"
#include "xmas.hpp"
using namespace std;

const int PLAY_TIME = 300000; //playback duration

SDL_Window* window = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Renderer* renderer = NULL;

void nightride(int playtime);

int main(int argc, char* args[])
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
	}
	else
	{
		window = SDL_CreateWindow("SDL2 Demo", SDL_WINDOWPOS_UNDEFINED, 
				SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 
				SDL_WINDOW_SHOWN);
		if(window == NULL)
		{
			cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
		}
		else
		{
			screenSurface = SDL_GetWindowSurface(window);
			//init the renderer
			renderer = SDL_CreateRenderer(window, -1, 
					SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if(renderer == NULL)
			{
				cout << "Renderer could not be created! SDL_Error: " << SDL_GetError() << endl;
			}
			else
			{
				//nightride(PLAY_TIME);
				glider(PLAY_TIME);
				//xmas(PLAY_TIME);
			}
			
			/*
			SDL_Rect chess[8][8];
			int a = 24;
			for(int i=0; i<8; i++)
			{
				for(int j=0; j<8; j++)
				{
					chess[i][j] = {.x = j*a, .y = i*a, .w = a, .h = a};
					if((i*7 + j) % 2)
					{
						SDL_FillRect(screenSurface, &chess[i][j], 
								SDL_MapRGB(screenSurface -> format, 0x00, 0x00, 0x00));
					}
					else
					{
						const short int white[3] = {0xFF, 0xFF, 0xFF};
						SDL_FillRect(screenSurface, &chess[i][j], 
								SDL_MapRGB(screenSurface -> format, white[0], white[1], 
									white[2]));
					}
				}
			}

			SDL_UpdateWindowSurface(window);
			SDL_Delay(1500);
			*/

		}
	}

	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}


