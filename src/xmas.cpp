/*  SDL Demo - xmas.cpp - fractal Christmas trees, stars and crackers!
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>
#include <math.h>
#include "global.hpp"
#include "quit.hpp"
using namespace std;

#define PI 3.141593

//-----------------------------------------------------------------------------

class Branch
{
	//the class for green fractal lines
	public:
		int sx, sy, //starting coordinates
			dir, //direction in degrees clockwise
			len, //length in pixels
			ex, ey; //ending coordinates

		Branch(int sx_, int sy_, int dir_, int len_); //the constructor
		void draw();
		void split(int fork_n, int div);
};



Branch::Branch(int sx_, int sy_, int dir_, int len_)
{
	//the constructor for the Branch class
	sx = sx_;
	sy = sy_;
	dir = dir_;
	len = len_;
	ex = sx + floor(len*sin(dir*PI / 180));
	ey = sy + floor(len*cos(dir*PI / 180));
}


void Branch::draw()
{
	//draw the branch on screen
	SDL_SetRenderDrawColor(renderer, colors[COLOR_WHITE][0], 
			colors[COLOR_WHITE][1], colors[COLOR_WHITE][2], 0xFF);
	SDL_RenderDrawLine(renderer, sx, sy, ex, ey);
	//update and wait
	SDL_RenderPresent(renderer);
	SDL_Delay(FRAME_DELAY);
}



void Branch::split(int fork_n, int div)
{
	//a recursive function to split the branch into sub-branches
	int ch_n = 3; //number of children
	Branch* triple[ch_n];
	int newlen = len;

	for(int j=0; j<fork_n; j++)
	{
		for(int k=0; k<ch_n; k++)
		{
			triple[k] = new Branch(ex, ey, dir + (k - 1)*div, newlen);
			triple[k]->draw();
		}
	}
	triple[1]->split(fork_n, div);
}


//-----------------------------------------------------------------------------

void xmas(int playtime)
{
	//the main function in this file
	Branch mainbranch(10, SCREEN_HEIGHT - 100, 120, 50);
	//draw everything rep times
	const short int rep = playtime / FRAME_DELAY;
	int k = 0; //counter
	while(k < rep && !quit())
	{
		//fill the window with black
		SDL_SetRenderDrawColor(renderer, colors[COLOR_BLACK][0], 
				colors[COLOR_BLACK][1], colors[COLOR_BLACK][2], 0xFF);
		SDL_RenderClear(renderer);
		mainbranch.draw();
		mainbranch.split(5, 30);

		k++;
	}
}
