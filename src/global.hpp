/*  SDL Demo - global.hpp - global variables
    Copyright (C) 2018-2020 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLOBAL_H
#define GLOBAL_H

//"screen" size (in fact, the window's one)
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//sleep time between frames
const int FRAME_DELAY = 40;
//the common size step
const short int size_step = 20;

extern SDL_Window* window;
extern SDL_Surface* screenSurface;
extern SDL_Renderer* renderer;

const int colors[][3] = {{0x00, 0x00, 0x00}, //COLOR_BLACK
                          {0xFF, 0xFF, 0xFF}, //COLOR_WHITE
						  {0x33, 0x33, 0x33}, //COLOR_DARKGRAY
						  {0x66, 0x66, 0x66}, //COLOR_LIGHTGRAY
						  {0x00, 0x00, 0x44}, //COLOR_DARKMAGENTA
						  {0xDD, 0xBB, 0x00}, //COLOR_DARKYELLOW
						  {0xFF, 0xFF, 0xAA}, //COLOR_LIGHTYELLOW
						  {0xAA, 0x00, 0x00}, //COLOR_DARKRED
						  {0xEE, 0x00, 0x00}, //COLOR_LIGHTRED
						  {0x33, 0xAA, 0x33} //COLOR_LIGHTGREEN
                         };
enum colornames {COLOR_BLACK, COLOR_WHITE, COLOR_DARKGRAY, COLOR_LIGHTGRAY, 
	COLOR_DARKMAGENTA, COLOR_DARKYELLOW, COLOR_LIGHTYELLOW, COLOR_DARKRED,
	COLOR_LIGHTRED, COLOR_LIGHTGREEN};

#endif
