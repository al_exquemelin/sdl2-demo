This project is a bunch of C++ files that draw some animations using SDL2, 
imitating "real" PC demos. They break most of the demoscene's rules, but still 
they look pretty good and don't need any bitmaps, so... what's left to be 
desired?

