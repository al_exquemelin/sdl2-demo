#   SDL Demo - the Makefile
#   Copyright (C) 2018-2019 Viktor Goryainov <al_exquemelin@yahoo.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or 
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, 
#   but WITHOUT ANY WARRANTY; without even the implied warranty of 
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License 
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

CC = g++
objects = sdl_demo.o nightride.o quit.o text.o glider.o xmas.o
debug: copts = -Wall -std=gnu++11 -c -g -DDEBUG
sdldemo: copts = -Wall -std=gnu++11 -c

ifeq ($(OS), Windows_NT)
	copts += -DWINDOWS
	lopts = -lmingw32 -lSDL2 -lSDL2_gfx -mwindows
	exec = sdl_demo.exe
else
	lopts = -lSDL2 -lSDL2_gfx
	exec = sdldemo
endif

vpath %.cpp src
vpath %.hpp src

sdldemo: $(objects)
	$(CC) $(objects) -Wall -o $@ $(lopts)

debug: $(objects)
	$(CC) $(objects) -Wall -g -o sdldemo $(lopts)

sdl_demo.o: sdl_demo.cpp nightride.hpp global.hpp glider.hpp xmas.cpp xmas.hpp
	$(CC) $(copts) $<

nightride.o: nightride.cpp global.hpp quit.hpp text.hpp
	$(CC) $(copts) $<

glider.o: glider.cpp global.hpp quit.hpp text.hpp
	$(CC) $(copts) $<
	
xmas.o: xmas.cpp global.hpp quit.hpp
	$(CC) $(copts) $<

quit.o: quit.cpp
	$(CC) $(copts) $<

text.o: text.cpp
	$(CC) $(copts) $<

clean: 
	-rm $(exec) $(objects)
